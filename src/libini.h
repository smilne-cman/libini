#ifndef __LIBINI__
#define __LIBINI__

/* Includes *******************************************************************/
#include <libcollection/map.h>

/* Types **********************************************************************/
typedef struct IniFile {
  const char *filename;
  Map *entries;
} IniFile;

/* Macros *********************************************************************/

/* Global Functions ***********************************************************/
IniFile *ini_open(const char *filename);
void ini_read(IniFile *file);
void ini_write(IniFile *file);
void ini_close(IniFile *file);

int ini_size(IniFile *file);
void ini_put(IniFile *file, const char *sectionName, const char *key, const char *value);
const char *ini_get(IniFile *file, const char *sectionName, const char *key);
void ini_remove(IniFile *file, const char *sectionName, const char *key);
Map *ini_section(IniFile *file, const char *sectionName);

#endif
