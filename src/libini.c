#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "libini.h"
#include <libcollection/iterator.h>
#include <libcollection/compare.h>
#include <libcollection/map.h>
#include <libstring/libstring.h>

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static const char *or(const char *a, const char *b);
static int is_comment(char *line);
static int is_section(char *line);
static int file_exists(const char *filename);

/* Global Variables ***********************************************************/

/* Functions ******************************************************************/
IniFile *ini_open(const char *filename) {
  IniFile *file = (IniFile *)malloc(sizeof(IniFile));

  file->filename = filename;
  file->entries = map_new(map_by_string(map_compare));

  return file;
}

void ini_read(IniFile *file) {
  if(file_exists(file->filename) == FALSE) return;

  char *currentSection = (char *)malloc(sizeof(char) * 120);
  strcpy(currentSection, "");

  FILE *handle = fopen(file->filename, "r");

  char *buffer = (char *)malloc(sizeof(char) * 255);
  while(fgets(buffer, 255, handle) != NULL) {
    if(is_comment(buffer)) continue;

    if(is_section(buffer)) {
      char *line = (char *)malloc(strlen(buffer + 1));
      strcpy(line, buffer);
      sscanf(line, "[%s", currentSection);
      currentSection[strlen(currentSection) - 1] = '\0';
      continue;
    }

    char *name = trim(split(buffer, "="));
    char *value = trim(strtok(NULL, "="));

    ini_put(file, currentSection, name, value);
  }

  free(buffer);
  fclose(handle);
}

void ini_write(IniFile *file) {
  FILE *handle = fopen(file->filename, "w");

  Iterator *sections = map_iterator(file->entries);
  while(has_next(sections)) {
    MapEntry *section = next_map(sections);
    if (strcmp(section->key, "")) {
      fprintf(handle, "\n[%s]\n", section->key);
    }

    Iterator *lines = map_iterator(section->value);
    while(has_next(lines)) {
      MapEntry *line = next_map(lines);

      fprintf(handle, "%s = %s\n", line->key, line->value);
    }

    iterator_destroy(lines);
  }

  iterator_destroy(sections);
  fclose(handle);
}

void ini_close(IniFile *file) {
  map_destroy(file->entries);
  free(file);
}

int ini_size(IniFile *file) {
  int count = 0;

  Iterator *sections = map_iterator(file->entries);
  while (has_next(sections)) {
    MapEntry *section = next_map(sections);

    count += map_size(section->value);
  }

  return count;
}

void ini_put(IniFile *file, const const char *sectionName, const char *key, const char *value) {
  Map *section = map_get(file->entries, or(sectionName, ""));

  if (section == NULL) {
    section = map_new(map_by_string(string_compare));
    map_put(file->entries, or(sectionName, ""),  section);
  }

  map_put(section, key, value);
}

const char *ini_get(IniFile *file, const char *sectionName, const char *key) {
  Map *section = map_get(file->entries, or(sectionName, ""));

  if (section == NULL) {
    return NULL;
  }

  return (const char *)map_get(section, key);
}

void ini_remove(IniFile *file, const char *sectionName, const char *key) {
  Map *section = map_get(file->entries, or(sectionName, ""));

  if (section == NULL) {
    return;
  }

  map_remove(section, key);
}

Map *ini_section(IniFile *file, const char *sectionName) {
  Map *section = map_get(file->entries, or(sectionName, ""));

  if (section == NULL) {
    return map_new(map_by_string(string_compare));
  }

  return section;
}

static const char *or(const char *a, const char *b) {
  if (a == NULL) {
    return b;
  }

  return a;
}

static int is_comment(char *line) {
  if(strlen(line) <= 1) return TRUE;

  return line[0] == '#' || line[0] == ';';
}

static int is_section(char *line) {
  return line[0] == '[';
}

static int file_exists(const char *filename) {
  FILE *file;
  if((file = fopen(filename, "r"))){
    fclose(file);

    return TRUE;
  }

  return FALSE;
}
