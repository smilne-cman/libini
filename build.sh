#!/bin/bash
set -e

echo "libini Build System"
echo ""

APPLICATION="libini"

SOURCE="./src"
BINARY="./bin"
DIRSTRIBUTION="./dist"
LIBRARY="./lib"
RESOURCE="./res"
TEST="./test"

# Show command help and options
if [ "$1" = "help" ]; then
	echo " clean                  -  Delete all build artifacts"
  echo " resources              -  Build resource bundle"
  echo " compile                -  Compiles the source code"
  echo " link                   -  Links the object code with the libraries"
	echo " test                   -  Run unit tests"
  echo " publish                -  Packages the executable in an archive"
	echo
	exit
fi

if [ "$1" = "clean" ] || [ -z $1 ]; then
  echo "Cleaning old build artifacts..."
  rm -fr $BINARY
  rm -fr $DIRSTRIBUTION
  rm -f $APPLICATION.a
fi

if [ "$1" = "resources" ] || [ -z $1 ]; then
  echo "Bundling resources..."
fi

if [ "$1" = "compile" ] || [ -z "$1" ]; then
  echo "Compiling..."
	mkdir bin
  cd $SOURCE
  gcc -c *.c -I.$LIBRARY
  cd ..
  mv $SOURCE/*.o $BINARY/
fi

if [ "$1" = "link" ] || [ -z "$1" ]; then
  echo "Linking..."
  cp $LIBRARY/**/*.a $BINARY/
	cd $BINARY
	for archive in *.a; do
		ar -x $archive
		rm $archive
	done
	cd ..
fi

if [ "$1" = "test" ] || [ -z "$1" ]; then
  echo "Running unit tests..."
	mkdir $BINARY/tests
	cd $TEST
	for filename in *.test.c; do
		gcc -I.$SOURCE -I.$LIBRARY -o ${filename%.c} $filename .$BINARY/*.o .$LIBRARY/**/*.a -w
		mv ${filename%.c} .$BINARY/tests/${filename%.test.c}
	done
	cd .$BINARY/tests
	for filename in *; do
		echo ""; echo ""
		echo "$filename:"
		./${filename}
	done
	echo ""
	cd ../../
fi

if [ "$1" = "publish" ] || [ -z "$1" ]; then
  echo "Packaging..."
	mkdir $DIRSTRIBUTION
	ar rcs $BINARY/$APPLICATION.a $BINARY/*.o
	cp $BINARY/$APPLICATION.a $DIRSTRIBUTION/
	cp $SOURCE/*.h $DIRSTRIBUTION/
	cd $DIRSTRIBUTION
	tar -cf $APPLICATION.tar $APPLICATION.a *.h
	gzip $APPLICATION.tar
	rm *.a *.h
fi
