#include <stdio.h>
#include <stdlib.h>
#include <libtest/libtest.h>
#include <libcollection/map.h>
#include "libini.h"

/* Types **********************************************************************/

/* Prototypes *****************************************************************/
static void before_each();
static void after_each();

static void test_open();
static void test_put();
static void test_put_section();
static void test_remove();
static void test_size();
static void test_write();
static void test_read();
static void test_read_no_file();
static void test_section();

/* Global Variables ***********************************************************/
static IniFile *file;

int main(int argc, char *argv[]) {
	test_init("libini", argc, argv);
  test_before_each(before_each);
  test_after_each(after_each);

	test("Open", test_open);
  test("Put", test_put);
  test("Put Section", test_put_section);
  test("Remove", test_remove);
  test("Size", test_size);
  test("Write", test_write);
  test("Read", test_read);
  test("Read (no file)", test_read_no_file);
  test("Section", test_section);

	return test_complete();
}

static void before_each() {
  file = ini_open("test.ini");
}

static void after_each() {
  ini_close(file);
}

static void test_open() {
  test_assert_string(file->filename, "test.ini", "Should set filename");
  test_assert_int(ini_size(file), 0, "Should create an empty map");
}

static void test_put() {
  ini_put(file, NULL, "name1", "value1");
  ini_put(file, NULL, "name2", "value2");
  ini_put(file, NULL, "name3", "value3");

  test_assert_int(ini_size(file), 3, "Should store entries");
  test_assert_string(ini_get(file, NULL, "name1"), "value1", "Should be able to retrieve first entry");
  test_assert_string(ini_get(file, NULL, "name2"), "value2", "Should be able to retrieve second entry");
  test_assert_string(ini_get(file, NULL, "name3"), "value3", "Should be able to retrieve third entry");
}

static void test_put_section() {
  ini_put(file, NULL, "name", "value");
  ini_put(file, "test", "name", "ABCabc");

  test_assert_int(ini_size(file), 2, "Should store entries");
  test_assert_string(ini_get(file, NULL, "name"), "value", "Should be able to retrieve entries from implicit default section");
  test_assert_string(ini_get(file, "", "name"), "value", "Should be able to retrieve entries from explicit default section");
  test_assert_string(ini_get(file, "test", "name"), "ABCabc", "Should be able to retrieve entries from named sections");
}

static void test_remove() {
  ini_put(file, NULL, "name1", "value1");
  ini_put(file, NULL, "name2", "value2");
  ini_put(file, NULL, "name3", "value3");

  test_assert_int(ini_size(file), 3, "SANITY -- Should have three entries");
  ini_remove(file, NULL, "name2");

  test_assert_int(ini_size(file), 2, "Should remove the entry");
  test_assert_string(ini_get(file, NULL, "name1"), "value1", "Should still contain first entry");
  test_assert_string(ini_get(file, NULL, "name3"), "value3", "Should still contain third entry");
}

static void test_size() {
  ini_put(file, NULL, "name1", "value1");
  ini_put(file, NULL, "name2", "value2");
  ini_put(file, NULL, "name3", "value3");

  test_assert_int(ini_size(file), 3, "Should keep track of size");
}

static void test_write() {
  ini_put(file, NULL, "name", "libini");
  ini_put(file, NULL, "version", "1.0.0");
  ini_put(file, NULL, "description", "Library to help work with .ini files");

  ini_put(file, "directories", "source", "src");

  ini_put(file, "library", "test", "1.0.0");
  ini_put(file, "library", "utility", "1.0.0");

  ini_write(file);
}

static void test_read() {
  ini_read(file);

  test_assert_int(ini_size(file), 6, "Should read all lines");
  test_assert_string(ini_get(file, NULL, "name"), "libini", "Should read 'name' entry");
  test_assert_string(ini_get(file, NULL, "version"), "1.0.0", "Should read 'version' entry");
  test_assert_string(ini_get(file, NULL, "description"), "Library to help work with .ini files", "Should read 'description' entry");

  test_assert_string(ini_get(file, "directories", "source"), "src", "Should read 'source' entry in 'directories' section");

  test_assert_string(ini_get(file, "library", "test"), "1.0.0", "Should read 'test' entry in 'library' section");
  test_assert_string(ini_get(file, "library", "utility"), "1.0.0", "Should read 'utility' entry in 'library' section");
}

static void test_read_no_file() {
  IniFile *file = ini_open("non-existing.ini");

  ini_read(file);
  test_assert_int(ini_size(file), 0, "Should not have any entries");
}

static void test_section() {
  ini_put(file, NULL, "key2", "something");
  ini_put(file, "my-section", "key1", "value1");
  ini_put(file, "my-section", "key2", "value2");
  ini_put(file, "my-section", "key3", "value3");

  Map *section = ini_section(file, "my-section");
  test_assert_int(map_size(section), 3, "Should return section with all entries");
  test_assert_string( (const char *)map_get(section, "key2"), "value2", "Section should contain data");

  Map *emptySection = ini_section(file, "blah");
  test_assert_int(map_size(emptySection), 0, "Should return an empty map when section does not exist");
}
